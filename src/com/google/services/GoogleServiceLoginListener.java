/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.services;

import com.android.tools.idea.ui.properties.core.BoolValueProperty;
import com.android.tools.idea.ui.properties.core.ObservableBool;
import com.google.gct.login.GoogleLogin;
import com.google.gct.login.GoogleLoginListener;
import com.intellij.openapi.extensions.Extensions;

/**
 * Listener which is notified when the user logs in or out of Google, exposed to classes in this
 * plugin through the {@link #loggedIn()} property.
 */
public final class GoogleServiceLoginListener implements GoogleLoginListener {
  public static GoogleServiceLoginListener getInstance() {
    return Extensions.findExtension(GoogleLoginListener.EP_NAME, GoogleServiceLoginListener.class);
  }

  private final BoolValueProperty loggedIn = new BoolValueProperty();

  public ObservableBool loggedIn() {
    return loggedIn;
  }

  @Override
  public void statusChanged() {
    loggedIn.set(GoogleLogin.getInstance().isLoggedIn());
  }
}
